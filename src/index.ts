import express from 'express';
import http from 'http';
import bodyParser from 'body-parser';
import mutantRouter from './routes/Mutant';

function loggerMiddleware(request: express.Request, response: express.Response, next: express.NextFunction) {
	console.log(`${request.method} ${request.path}`);
  next();
}

/**
   * Setup server
*/
const app = express();
const port = process.env.SERVER_PORT || 8080;

app.use(loggerMiddleware);
app.use(bodyParser.json())
app.use('/', mutantRouter);

http.createServer(app).listen(port);
console.log(`⚡️[server]: Server is running at http://localhost:${port}`);
