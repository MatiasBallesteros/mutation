const MUTATION_MIN:number = 2;
let cantMutation:number = 0;

export function makeMatrizByArray(array: String) {
    let arrayLenght = array.length;
    let matriz = []
    for (let i = 0; i < arrayLenght; i++) {
        let dna: String = array[i];
        matriz[i] = dna;
    }
    return matriz;
}

export function isMutan(dna: []): Boolean {
    cantMutation=0;
    let mutantSequences: string[] = ["AAAA", "TTTT", "CCCC", "GGGG"];
    for (let i = 0; i < mutantSequences.length; i++) {
        findInSequence(dna, mutantSequences[i])
    }
    if (cantMutation >= MUTATION_MIN) {
       return true;
    }
    return false;
}

export function findInSequence(dna: String[], sequence: string) {

    for (let i = 0; i < dna.length; i++) {
        // find horizontal
        if (dna[i].includes(sequence)) {
            cantMutation++;
        }

        //compose the vertical diagonal
        let sequenceToAnalyze: String = "";
        for (let j = 0; j < dna.length; j++) {
            sequenceToAnalyze += dna[j].charAt(i);
        }

        // find vertical
        if (sequenceToAnalyze.includes(sequence)) {
            cantMutation++;
        }
    }

    //I calculate the difference between the dimension of the matrix and the length of the sequence to avoid unnecessary for in the diagonals
    let length = dna.length - sequence.length;

    findInLoweAndCentralDiagonalFromTopBottom(length, dna, sequence);
    findInUpperDiagonalFromTopBottom(length, dna, sequence);
    findInLowerAndCentralDiagonalFromBottomUp(length, dna, sequence);
    findInUpperDiagonalFromBottomUp(length, dna, sequence);

}

//find in the lower and central diagonal, from top to bottom
function findInLoweAndCentralDiagonalFromTopBottom(length: number, dna: String[], sequence: string) {
    for (let i = length; i >= 0; i--) {
        let sequenceToAnalyze: String = "";
        for (let j = 0; j < dna.length - i; j++) {
            sequenceToAnalyze += dna[i + j].charAt(j);
        }
        sequenceInclude(sequenceToAnalyze, sequence);
    }
}

//find in the upper diagonal, from top to bottom
function findInUpperDiagonalFromTopBottom(length: number, dna: String[], sequence: string) {
    for (let i = 1; i <= length; i++) {
        let sequenceToAnalyze: String = "";
        for (let j = 0; j < dna.length - i; j++) {
            sequenceToAnalyze += dna[j].charAt(i + j);
        }
        sequenceInclude(sequenceToAnalyze, sequence);
    }
}

//I look for the occurrences in the lower diagonal and central diagonal, from the bottom up
function findInLowerAndCentralDiagonalFromBottomUp(length: number, dna: String[], sequence: string) {
    for (let i = length + 1; i < dna.length; i++) {
        let sequenceToAnalyze: String = "";
        for (let j = 0; j <= i; j++) {
            sequenceToAnalyze += dna[i - j].charAt(j);
        }
        sequenceInclude(sequenceToAnalyze, sequence);

    }
}

//I look for the occurrences in the upper diagonal, from the bottom up
function findInUpperDiagonalFromBottomUp(length: number, dna: String[], sequence: string) {
    for (let i = 0; i < length; i++) {
        let sequenceToAnalyze: String = "";
        for (let j = i + 1; j < dna.length; j++) {
            sequenceToAnalyze += dna[dna.length - j + i].charAt(j);
        }
        sequenceInclude(sequenceToAnalyze, sequence);
    }
}

function sequenceInclude(sequenceToAnalyze: String, sequence: string) {
    if (sequenceToAnalyze.includes(sequence)) {
        cantMutation++;
    }
}

