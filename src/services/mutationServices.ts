import { isMutan } from '../utils/matriz';
import Dna from '../model/dna';
import Stats from '../model/stats';

/**
 * check for a mutation and save statistics
 */
export async function hasMutation(dna: []) {
    let isMutation: Boolean = false;
    if (isMutan(dna)) {
        isMutation = true;
        let dnaToSave: String = dna.join('');
        try {
            await Dna.create({ dna: dnaToSave });
            await Stats.findOneAndUpdate({ name: "statistics" }, { $inc: { count_mutations: 1 } }, { new: true })
        } catch (error) {
            console.error(error);
        }
    } else {
        try {
            await Stats.findOneAndUpdate({ name: "statistics" }, { $inc: { count_no_mutation: 1 } }, { new: true })
        } catch (error) {
            console.error(error);
        }
    }
    return isMutation;
}
