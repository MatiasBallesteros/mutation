import { IStatResponse } from './../model/stats';
import Stats from '../model/stats';

/**
   * find stats and calculate ratio
*/
export async function findStats() {

    let stat = await Stats.findOne({ name: "statistics" });
    if (stat != null) {
        let count_mutations: Number = stat?.count_mutations != null ? stat?.count_mutations : 0;
        let count_no_mutation: Number = stat?.count_no_mutation != null ? stat?.count_no_mutation : 0;

        let statResponse: IStatResponse | undefined;
        ({ statResponse } = calculateRatio(count_mutations, count_no_mutation));
        return statResponse;
    } else {
        return null;
    }
}

/**
   * calculates the ratio between two values
*/
function calculateRatio(count_mutations: Number, count_no_mutation: Number) {
    let ratio: Number = 0;
    if (count_mutations > count_no_mutation && count_mutations > 0) {
        ratio = Math.round((count_no_mutation.valueOf() / count_mutations.valueOf()) * 100.0) / 100.0;
    } else if(count_no_mutation > 0) {
        ratio = Math.round((count_mutations.valueOf() / count_no_mutation.valueOf()) * 100.0) / 100.0;
    }

    let statResponse: IStatResponse = {
        count_mutations: count_mutations,
        count_no_mutation: count_no_mutation,
        ratio: ratio,
    };
    return { statResponse };
}

