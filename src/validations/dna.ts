export function validateCorrectDna(dnaToSave: string): Boolean {
    return (/^[ACGT]*$/g).test(dnaToSave);
  }
