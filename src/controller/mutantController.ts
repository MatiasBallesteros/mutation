import express from 'express';
import { hasMutation } from '../services/mutationServices';
import { validateCorrectDna } from '../validations/dna';


export async function mutation(req: express.Request, res: express.Response){
    let dna = req.body.dna;
    let dnaToSave: string = dna.join('');
    let isMutation: Boolean = false;
    // mutation min is 4 equal letters
    if (dna == null || dna.length < 4){
        res.status(403).send("wrong information");
        return;
    }
    if (!validateCorrectDna(dnaToSave)){
        res.status(403).send("wrong character in dna");
        return;
    }
    isMutation = await hasMutation(dna);
    if (isMutation){
        res.status(200).send("OK");
        return;
    }else {
        res.status(403).send(isMutation);
        return;
    }
}


