import express from 'express';
import { findStats } from '../services/statService';

export async function stats(req: express.Request, res: express.Response){
    let stat = await findStats();
    if (stat != null){
        res.status(200).send(stat);
    }else{
        res.status(403).send('No data available');
    }
}