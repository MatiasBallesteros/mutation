import express from 'express';
import { mutation } from '../controller/mutantController';
import { stats } from '../controller/statsController';

const router = express.Router();

router.post("/mutation", mutation);
router.get("/stats", stats);


export default router;
