import mongoose from '../config/moongose/index';

//create interface if necessary in the future (example in model stats)

const dna = {
    dnaSchema: new mongoose.Schema({
        dna: {
            type: String,
            required: true,
            unique: true,
        },
    })
};

export default mongoose.model('Dna', dna.dnaSchema, 'dna');