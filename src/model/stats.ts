import { Document } from 'mongoose';
import mongoose from '../config/moongose/index';

export interface IStats extends Document {
    count_mutations: Number;
    count_no_mutation: Number;
    name: string;
}

export interface IStatResponse{
    count_mutations: Number;
    count_no_mutation: Number;
    ratio: Number;
}

const stats = {
    statSchema: new mongoose.Schema({
        count_mutations: {
            type: Number,
        },
        count_no_mutation: {
            type: Number,
        },
        name: {
            type: String,
            unique: true,
        }
    })
};


export default mongoose.model<IStats>('Stats', stats.statSchema, 'stats');