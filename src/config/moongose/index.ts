import mongoose from 'mongoose';

//add user pass and bd in enviroment variable
const MONGO_DB_CONNECTION_STRING = `mongodb+srv://admin:admin@cluster0.bpgbq.mongodb.net/mutation?retryWrites=true&w=majority`;

mongoose.connect(MONGO_DB_CONNECTION_STRING, { useNewUrlParser: true, useUnifiedTopology: true }, (err) => {
    if (err)
        console.error(err);
    else
        console.log("Connected to the mongodb");
});
mongoose.set('useCreateIndex', true);
mongoose.set('useFindAndModify', false);

export default mongoose;

