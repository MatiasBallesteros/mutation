# Instrucciones de uso

El servicio se encuentra deployado en https://mutationperson.rj.r.appspot.com

#   Servicio en la nube
## Se tienen dos metodos actualmente en funcionamiento:

### Método POST para saber si un ADN dado es pertenece a un mutante:

La URL del método es https://mutationperson.rj.r.appspot.com/mutation . Se puede saber si un humano es mutante enviando la secuencia de ADN mediante un HTTP POST con un JSON el cual tenga el siguiente formato:

POST → /mutation/
{
  "dna":["TTGCGA","ACTGGC","ATCTGT","ACACCG","GCGTTA","TCTCTG"]
}

En caso de verificar que el ADN enviado es mutante, el método devuelve como respuesta un HTTP 200-OK, en caso contrario un 403-Forbidden
### Metodo GET que nos devuelve estadisticas de las verificaciones de ADN:

La URL del método es [https://mutationperson.rj.r.appspot.com/stats]

#  Servicio local
* Para buildear el proyecto utilizamos el comando 'npm run build'
* Tambien se puede utilizar el sistema localmente corriendo el script de la siguiente manera(en la carpeta raiz del proyecto): ./run_dev.sh

## Método POST para saber si un ADN dado es pertenece a un mutante:
    La URL del metodo es localhost:8080/mutation y funciona de igual manera que el metodo en la nube

## Metodo GET que nos devuelve estadisticas de las verificaciones de ADN:
    La URL del metodo es localhost:8080/stats y funciona de igual manera que el metodo en la nube
## Cosas a mejorar

* Se deberia tipar todos los datos un ejemplo claro es el req en el mutantController
* Se deberia manejar clases de Response y/o tambien de Request
* Se deberia testear el codigo
* Se deberia tener un manejo de excepciones

